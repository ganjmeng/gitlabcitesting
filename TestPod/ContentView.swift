//
//  ContentView.swift
//  TestPod
//
//  Created by Gan Jingmeng on 2/12/19.
//  Copyright © 2019 Gan Jingmeng. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
