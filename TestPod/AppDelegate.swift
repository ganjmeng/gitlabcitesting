//
//  AppDelegate.swift
//  TestPod
//
//  Created by Gan Jingmeng on 2/12/19.
//  Copyright © 2019 Gan Jingmeng. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setupGoogleAPI()
        
        // 获取Url
        let session = URLSession(configuration: .default)
         let url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyDvuuuZgWDfwQdhU3t2D8Nj8Toyxou2T9U&destinations=1.313%2C103.854&origins=1.298427%2C103.782591"
         let  request = URLRequest(url: URL(string: url)!)
         let task = session.dataTask(with: request) {(data, response, error) in
             do {
                 let r = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                 print(r)
             } catch {
                 print("无法连接到服务2222222器发发发77777")
                 return
             }
         }
         task.resume()
        return true
    }
    
    // MARK: - Google API Setup
    func setupGoogleAPI() {
        GMSServices.provideAPIKey("AIzaSyDvuuuZgWDfwQdhU3t2D8Nj8Toyxou2T9U")
        GMSPlacesClient.provideAPIKey("AIzaSyDvuuuZgWDfwQdhU3t2D8Nj8Toyxou2T9U")

    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

